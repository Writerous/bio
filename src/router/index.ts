import { createRouter, createWebHistory } from 'vue-router'
import PassageView from '../views/PassageView.vue'
import SchoolPhotoView from "@/views/SchoolPhotoView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'passage',
      component: PassageView
    },
    {
      path: '/video',
      name: 'video',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/VideoView.vue')
    },
    {
      path: '/school',
      name: 'school',
      component: SchoolPhotoView
    }
  ]
})

export default router
